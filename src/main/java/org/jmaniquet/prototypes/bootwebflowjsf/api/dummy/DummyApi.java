package org.jmaniquet.prototypes.bootwebflowjsf.api.dummy;

import org.springframework.stereotype.Service;

@Service("dummy")
public class DummyApi {

	public void onFlowStart() {
		System.out.println("DUMMY - on flow start");
	}
	
	public void onViewEntry() {
		System.out.println("DUMMY - on view entry");
	}
	
	public void onViewRender() {
		System.out.println("DUMMY - on view render");
	}
	
	public void onTransitionFinish() {
		System.out.println("DUMMY - on transition finish");
	}
	
	public void onTransitionCancel() {
		System.out.println("DUMMY - on transition cancel");
	}
	
	public void onViewExit() {
		System.out.println("DUMMY - on view exit");
	}
	
	public void onEndEntry(String stateName) {
		System.out.println("DUMMY - on end entry - " + stateName);
	}
	
	public void onFlowEnd() {
		System.out.println("DUMMY - on flow end");
	}
}
