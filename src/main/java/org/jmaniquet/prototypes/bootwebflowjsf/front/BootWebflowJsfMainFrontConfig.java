package org.jmaniquet.prototypes.bootwebflowjsf.front;

import java.util.List;

import javax.faces.webapp.FacesServlet;
import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.view.InternalResourceView;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

@Configuration
public class BootWebflowJsfMainFrontConfig {
	
	// Mandatory if using classic @Controller with no flows
	// When using web mvc autoconfig, this bean can be removed with correct use of spring.mvc.view.prefix/spring.mvc.view.suffix properties
	// @Bean
	public UrlBasedViewResolver faceletsViewResolver() {
		UrlBasedViewResolver vr = new UrlBasedViewResolver();
		vr.setViewClass(InternalResourceView.class);
		vr.setPrefix("/WEB-INF/ui");
		vr.setSuffix(".xhtml");
		return vr;
	}
	
	// Mandatory if not using joinfaces
	// @Bean
	public com.sun.faces.config.ConfigureListener mojarraConfigureListener() {
		return new com.sun.faces.config.ConfigureListener();
	}
	
	// Mandatory if not using joinfaces
	// @Bean
	public ServletRegistrationBean<FacesServlet> facesServletRegistrationBean() {
		ServletRegistrationBean<FacesServlet> jsfRegistrationBean = new ServletRegistrationBean<FacesServlet>(new FacesServlet()) {
			@Override
			protected ServletRegistration.Dynamic addRegistration(String description, ServletContext servletContext) {
				ServletRegistration.Dynamic servletRegistration = super.addRegistration(description, servletContext);
				if (servletRegistration != null) {
					servletContext.setAttribute("org.apache.myfaces.DYNAMICALLY_ADDED_FACES_SERVLET", true);
					servletContext.setAttribute("com.sun.faces.facesInitializerMappingsAdded", true);
				}
				return servletRegistration;
			}
		};
		
		// Default values from joinfaces
		// see : https://docs.joinfaces.org/current/reference/#properties
		// section : joinfaces.faces-servlet.*
		//jsfRegistrationBean.setAsyncSupported(true);
		//jsfRegistrationBean.setEnabled(true);
		//jsfRegistrationBean.setLoadOnStartup(-1);
		//jsfRegistrationBean.setName("FacesServlet");
		//jsfRegistrationBean.setOrder(0);
		
		// Only this one seems mandatory - activating the other properties won't break anything though
		jsfRegistrationBean.setUrlMappings(List.of("/faces/*", "*.jsf", "*.faces", "*.xhtml"));

		return jsfRegistrationBean;
	}
}
