package org.jmaniquet.prototypes.bootwebflowjsf.front;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Configuration;
import org.springframework.lang.Nullable;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

@Configuration
public class MandatoryBeansConfig implements ApplicationContextAware, ServletContextAware {
	
	private WebMvcConfigurationSupport delegate = new WebMvcConfigurationSupport();
	
	@Nullable
	private ApplicationContext applicationContext;

	@Nullable
	private ServletContext servletContext;
	
	@PostConstruct
	public void init() {
		delegate.setApplicationContext(applicationContext);
		delegate.setServletContext(servletContext);
	}
	
	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}
}
