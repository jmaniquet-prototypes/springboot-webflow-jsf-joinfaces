package org.jmaniquet.prototypes.bootwebflowjsf.front;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.support.FormattingConversionService;
import org.springframework.http.MediaType;
import org.springframework.lang.Nullable;
import org.springframework.validation.Validator;
import org.springframework.web.HttpRequestHandler;
import org.springframework.web.accept.ContentNegotiationManager;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.springframework.web.servlet.mvc.HttpRequestHandlerAdapter;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;
import org.springframework.web.servlet.resource.ResourceUrlProvider;

@Configuration
public class MandatoryBeansWhenExcludingMvcAutoConfig implements ApplicationContextAware, ServletContextAware {
	
	private static final Logger logger = LoggerFactory.getLogger(MandatoryBeansWhenExcludingMvcAutoConfig.class);
	
	private WebMvcConfigurationSupport delegate = new WebMvcConfigurationSupport();
	
	@Nullable
	private ApplicationContext applicationContext;

	@Nullable
	private ServletContext servletContext;
	
	@Autowired
	private List<RequestMappingHandlerMapping> requestMappingHandlerMappings;
	@Autowired
	private List<ContentNegotiationManager> contentNegotiationManagers;
	@Autowired
	private List<ResourceUrlProvider> resourceUrlProviders;
	@Autowired
	private List<RequestMappingHandlerAdapter> requestMappingHandlerAdapters;
	@Autowired
	private List<FormattingConversionService> formattingConversionServices;
	@Autowired
	private List<Validator> dummyList;
	@Autowired
	private List<HttpRequestHandlerAdapter> httpRequestHandlerAdapters;
	@Autowired
	private List<HttpRequestHandler> httpRequestHandlers;

	@SuppressWarnings("deprecation")
	@PostConstruct
	public void init() {
		delegate.setApplicationContext(applicationContext);
		delegate.setServletContext(servletContext);
		
		for (RequestMappingHandlerMapping underObserve : requestMappingHandlerMappings) {
			logger.info("[class={}][defaultHandlerClass={}][order={}][useSuffixPatternMatch={}][useRegisteredSuffixPatternMatch={}][useTrailingSlashMatch={}][pathPrefixes={}][handlerMethods={}]",
					underObserve.getClass(),
					underObserve.getDefaultHandler(),
					underObserve.getOrder(),
					underObserve.useSuffixPatternMatch(),
					underObserve.useRegisteredSuffixPatternMatch(),
					underObserve.useTrailingSlashMatch(),
					underObserve.getPathPrefixes());
			logger.info("[handlerMethods={}]", underObserve.getHandlerMethods());
		}
		logger.info("");
		for (ContentNegotiationManager underObserve : contentNegotiationManagers) {
			logger.info("[class={}][fileExtensions={}][mediaTypeMappings={}][strategies={}]", underObserve.getClass(), underObserve.getAllFileExtensions(), underObserve.getMediaTypeMappings(), underObserve.getStrategies());
		}
		logger.info("");
		for (ResourceUrlProvider underObserve : resourceUrlProviders) {
			logger.info("[class={}][handlerMap={}]", underObserve.getClass(), underObserve.getHandlerMap());
		}
		logger.info("");
		for (RequestMappingHandlerAdapter underObserve : requestMappingHandlerAdapters) {
			logger.info("[class={}][cacheControl={}][cacheSeconds={}][order={}][supportedMethods={}][customArgumentResolvers={}][reactiveAdapterRegistry={}][webBindingInitializer={}]",
					underObserve.getClass(),
					underObserve.getCacheControl(),
					underObserve.getCacheSeconds(),
					underObserve.getOrder(),
					underObserve.getSupportedMethods(),
					underObserve.getCustomArgumentResolvers(),
					underObserve.getReactiveAdapterRegistry(),
					underObserve.getWebBindingInitializer());
			
			// TODO
			underObserve.getCustomReturnValueHandlers();
			underObserve.getInitBinderArgumentResolvers();
			underObserve.getMessageConverters();
			underObserve.getModelAndViewResolvers();
			underObserve.getReturnValueHandlers();
		}
		logger.info("");
		for (FormattingConversionService underObserve : formattingConversionServices) {
			logger.info("[class={}]", underObserve.getClass());
		}
		logger.info("");
		for (Validator underObserve : dummyList) {
			logger.info("[class={}]", underObserve.getClass());
		}
		logger.info("");
		for (HttpRequestHandlerAdapter underObserve : httpRequestHandlerAdapters) {
			logger.info("[class={}]", underObserve.getClass());
		}
		logger.info("");
		for (HttpRequestHandler underObserve : httpRequestHandlers) {
			logger.info("[class={}]", underObserve.getClass());
		}
	}
	
	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}

	/**
	 * Return a {@link RequestMappingHandlerMapping} ordered at 0 for mapping
	 * requests to annotated controllers.
	 */
	//@Bean("fdsfs")
	public RequestMappingHandlerMapping requestMappingHandlerMapping(
			@Qualifier("mvcContentNegotiationManager") ContentNegotiationManager contentNegotiationManager,
			@Qualifier("mvcConversionService") FormattingConversionService conversionService,
			@Qualifier("mvcResourceUrlProvider") ResourceUrlProvider resourceUrlProvider) {
		RequestMappingHandlerMapping bean = delegate.requestMappingHandlerMapping(contentNegotiationManager, conversionService, resourceUrlProvider);
		//bean.setUseSuffixPatternMatch(false);
		return bean;
	}

	/**
	 * Return a {@link ContentNegotiationManager} instance to use to determine
	 * requested {@linkplain MediaType media types} in a given request.
	 */
	//@Bean
	public ContentNegotiationManager mvcContentNegotiationManager() {
		ContentNegotiationManager bean = delegate.mvcContentNegotiationManager();
		/*ServletPathExtensionContentNegotiationStrategy hack = bean.getStrategy(ServletPathExtensionContentNegotiationStrategy.class);
		bean.getStrategies().remove(hack);*/
		return bean;
	}

	/**
	 * A {@link ResourceUrlProvider} bean for use with the MVC dispatcher.
	 * @since 4.1
	 */
	//@Bean("hjhgjgh")
	public ResourceUrlProvider mvcResourceUrlProvider() {
		return delegate.mvcResourceUrlProvider();
	}

	/**
	 * Returns a {@link RequestMappingHandlerAdapter} for processing requests
	 * through annotated controller methods. Consider overriding one of these
	 * other more fine-grained methods:
	 * <ul>
	 * <li>{@link #addArgumentResolvers} for adding custom argument resolvers.
	 * <li>{@link #addReturnValueHandlers} for adding custom return value handlers.
	 * <li>{@link #configureMessageConverters} for adding custom message converters.
	 * </ul>
	 */
	//@Bean("gdfghfd")
	public RequestMappingHandlerAdapter requestMappingHandlerAdapter(
			@Qualifier("mvcContentNegotiationManager") ContentNegotiationManager contentNegotiationManager,
			@Qualifier("mvcConversionService") FormattingConversionService conversionService,
			@Qualifier("mvcValidator") Validator validator) {
		return delegate.requestMappingHandlerAdapter(contentNegotiationManager, conversionService, validator);
	}

	/**
	 * Return a {@link FormattingConversionService} for use with annotated controllers.
	 * <p>See {@link #addFormatters} as an alternative to overriding this method.
	 */
	//@Bean("hhgfhjfg")
	public FormattingConversionService mvcConversionService() {
		return delegate.mvcConversionService();
	}

	/**
	 * Return a global {@link Validator} instance for example for validating
	 * {@code @ModelAttribute} and {@code @RequestBody} method arguments.
	 * Delegates to {@link #getValidator()} first and if that returns {@code null}
	 * checks the classpath for the presence of a JSR-303 implementations
	 * before creating a {@code OptionalValidatorFactoryBean}.If a JSR-303
	 * implementation is not available, a no-op {@link Validator} is returned.
	 */
	//@Bean("hfghfg")
	public Validator mvcValidator() {
		return delegate.mvcValidator();
	}

	/**
	 * Returns a {@link HttpRequestHandlerAdapter} for processing requests
	 * with {@link HttpRequestHandler HttpRequestHandlers}.
	 * 
	 * App seems to work without it.
	 * Event the tests are green.
	 * However the flow tests log an exception :
	 * javax.servlet.ServletException: No adapter for handler [org.springframework.faces.webflow.JsfResourceRequestHandler@70fe181d]: The DispatcherServlet configuration needs to include a HandlerAdapter that supports this handler
	 */
	//@Bean("hgfhjfg")
	public HttpRequestHandlerAdapter httpRequestHandlerAdapter() {
		return delegate.httpRequestHandlerAdapter();
	}
}
