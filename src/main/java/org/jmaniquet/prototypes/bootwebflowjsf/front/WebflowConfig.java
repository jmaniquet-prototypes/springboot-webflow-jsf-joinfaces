package org.jmaniquet.prototypes.bootwebflowjsf.front;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.faces.config.FlowBuilderServicesBuilder;
import org.springframework.faces.config.ResourcesBeanDefinitionParser;
import org.springframework.faces.webflow.FlowFacesContextLifecycleListener;
import org.springframework.faces.webflow.JsfFlowHandlerAdapter;
import org.springframework.faces.webflow.JsfResourceRequestHandler;
import org.springframework.util.ClassUtils;
import org.springframework.web.HttpRequestHandler;
import org.springframework.web.servlet.HandlerAdapter;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.servlet.handler.SimpleUrlHandlerMapping;
import org.springframework.webflow.config.FlowDefinitionRegistryBuilder;
import org.springframework.webflow.config.FlowExecutorBuilder;
import org.springframework.webflow.definition.registry.FlowDefinitionRegistry;
import org.springframework.webflow.engine.builder.support.FlowBuilderServices;
import org.springframework.webflow.execution.FlowExecutionListener;
import org.springframework.webflow.executor.FlowExecutor;
import org.springframework.webflow.mvc.servlet.FlowHandlerMapping;

@Configuration
public class WebflowConfig implements ApplicationContextAware {

	private ApplicationContext applicationContext;
	
	@Bean
	public FlowExecutionListener facesContextListener() {
		return new FlowFacesContextLifecycleListener();
	}
	
	@Bean
	public FlowBuilderServices flowBuilderServices() {
		return new FlowBuilderServicesBuilder().build();
	}
	
	@Bean
	public FlowDefinitionRegistry flowRegistry() {
		return new FlowDefinitionRegistryBuilder(this.applicationContext, flowBuilderServices())
				.setBasePath("/WEB-INF/ui")
				.addFlowLocationPattern("/**/*-flow.xml")
				.build();
	}
	
	@Bean
	public FlowExecutor flowExecutor() {
		return new FlowExecutorBuilder(flowRegistry())
				.addFlowExecutionListener(facesContextListener())
				.build();
	}
	
	private static final boolean isRichFacesPresent = ClassUtils.isPresent("org.richfaces.application.CoreConfiguration", ResourcesBeanDefinitionParser.class.getClassLoader());
	
	@Bean
	public HandlerMapping jsfResourceHandlerMapping() {
		Map<String, Object> urlMap = new HashMap<>();
		urlMap.put("/javax.faces.resource/**", jsfResourceRequestHandler());
		if (isRichFacesPresent) {
			urlMap.put("/rfRes/**", jsfResourceRequestHandler());
		}

		SimpleUrlHandlerMapping handlerMapping = new SimpleUrlHandlerMapping();
		handlerMapping.setUrlMap(urlMap);
		handlerMapping.setOrder(0);
		//handlerMapping.setOrder(1); Works too
		return handlerMapping;
	}
	@Bean
	public HttpRequestHandler jsfResourceRequestHandler() {
		return new JsfResourceRequestHandler();
	}
	
	@Bean
	public HandlerMapping flowHandlerMapping() {
		FlowHandlerMapping m = new FlowHandlerMapping();
		m.setFlowRegistry(flowRegistry());
		m.setOrder(0);
		//m.setOrder(1); Works too
		return m;
	}
	@Bean
	public HandlerAdapter flowHandlerAdapter() {
		JsfFlowHandlerAdapter a = new JsfFlowHandlerAdapter();
		a.setFlowExecutor(flowExecutor());
		return a;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}
}
