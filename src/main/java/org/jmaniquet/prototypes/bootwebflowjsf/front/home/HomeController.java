package org.jmaniquet.prototypes.bootwebflowjsf.front.home;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

	public static final String URL_ROOT = "/";
	public static final String URL_LOGIN = "/login";
	public static final String URL_WELCOME = "/welcome";
	public static final String URL_HOME = "/home";
	
	@GetMapping({URL_ROOT, URL_LOGIN, URL_WELCOME, URL_HOME})
	public String welcome() {
		return "/home";
	}
}
