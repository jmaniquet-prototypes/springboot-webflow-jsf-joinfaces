package org.jmaniquet.prototypes.bootwebflowjsf.front;

import static org.assertj.core.api.Assertions.assertThat;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ProtoNavigator {
	
	private WebDriver webDriver;
	
	ProtoNavigator(WebDriver webDriver) {
		this.webDriver = webDriver;
	}
	
	void open(String rootPath) {
		webDriver.get(rootPath);
	}
	
	void open(String rootPath, String pagePath) {
		webDriver.get(rootPath + pagePath);
	}
	
	void goToAccount() {
		WebElement el = webDriver.findElement(By.id("main-form:go-to-account-btn"));
		el.click();
	}
	
	void clickFinish() {
		WebElement el = webDriver.findElement(By.id("main-form:finish-btn"));
		el.click();
	}
	
	void clickCancel() {
		WebElement el = webDriver.findElement(By.id("main-form:cancel-btn"));
		el.click();
	}
	
	void assertCurrentTitleIs(String expectedTitle) {
		String actual = webDriver.getTitle();
		assertThat(actual).isEqualTo(expectedTitle);
	}
}
