package org.jmaniquet.prototypes.bootwebflowjsf.front;

import static org.jmaniquet.prototypes.bootwebflowjsf.front.home.HomeController.URL_HOME;
import static org.jmaniquet.prototypes.bootwebflowjsf.front.home.HomeController.URL_LOGIN;
import static org.jmaniquet.prototypes.bootwebflowjsf.front.home.HomeController.URL_ROOT;
import static org.jmaniquet.prototypes.bootwebflowjsf.front.home.HomeController.URL_WELCOME;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

@SpringBootTest(webEnvironment = WebEnvironment.DEFINED_PORT)
public class ProtoTest {
	
	@Value("http://localhost:${server.port}")
	private String rootPath;
	private WebDriver webDriver;	
	private ProtoNavigator underTest;
	
	@BeforeEach
	public void beforeEach() {
		webDriver = new HtmlUnitDriver();
		underTest = new ProtoNavigator(webDriver);
	}
	
	@AfterEach
	public void afterEach() {
		webDriver.quit();
	}
	
	@ParameterizedTest
	@ValueSource(strings = {URL_ROOT, URL_LOGIN, URL_WELCOME, URL_HOME})
	public void testGoToHome(String t) {
		underTest.open(rootPath, "/app" +  t);
		underTest.assertCurrentTitleIs("Java Proto - boot/webflow/jsf/primfaces/joinfaces - Home");
	}
	
	@Test
	public void testGoToAccountAdminDirectlyAndFinish() {
		underTest.open(rootPath, "/app/account");
		underTest.assertCurrentTitleIs("Java Proto - boot/webflow/jsf/primfaces/joinfaces - Account admin");
		
		underTest.clickFinish();
		underTest.assertCurrentTitleIs("Google");
	}
	
	@Test
	public void testGoToAccountAdminDirectlyAndCancel() {
		underTest.open(rootPath, "/app/account");
		underTest.assertCurrentTitleIs("Java Proto - boot/webflow/jsf/primfaces/joinfaces - Account admin");
		
		underTest.clickCancel();
		underTest.assertCurrentTitleIs("Java Proto - boot/webflow/jsf/primfaces/joinfaces - Home");
	}
	
	@ParameterizedTest
	@ValueSource(strings = {URL_ROOT, URL_LOGIN, URL_WELCOME, URL_HOME})
	public void testGoToHomeAndFollowWithFlowWithFinish(String t) {
		underTest.open(rootPath, "/app" +  t);
		underTest.assertCurrentTitleIs("Java Proto - boot/webflow/jsf/primfaces/joinfaces - Home");
		
		underTest.goToAccount();
		underTest.assertCurrentTitleIs("Java Proto - boot/webflow/jsf/primfaces/joinfaces - Account admin");
		
		underTest.clickFinish();
		underTest.assertCurrentTitleIs("Google");
	}
	
	@ParameterizedTest
	@ValueSource(strings = {URL_ROOT, URL_LOGIN, URL_WELCOME, URL_HOME})
	public void testGoToHomeAndFollowWithFlowWithCancel(String t) {
		underTest.open(rootPath, "/app" +  t);
		underTest.assertCurrentTitleIs("Java Proto - boot/webflow/jsf/primfaces/joinfaces - Home");
		
		underTest.goToAccount();
		underTest.assertCurrentTitleIs("Java Proto - boot/webflow/jsf/primfaces/joinfaces - Account admin");
		
		underTest.clickCancel();
		underTest.assertCurrentTitleIs("Java Proto - boot/webflow/jsf/primfaces/joinfaces - Home");
	}
}
